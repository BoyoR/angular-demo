import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  lang: string = 'mn';
  visible: boolean = false;
  heroes!: Hero[];
  selectedHero?: Hero;

  constructor(private heroService: HeroService) {
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes().subscribe(hero => {
      this.heroes = hero;
    });
  };

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    console.log("onSelected hero: " + this.selectedHero)
  }
}
